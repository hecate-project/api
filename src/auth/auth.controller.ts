import { Controller, Post, UseGuards } from '@nestjs/common';
import {
  ApiBody,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { UnauthorizedDto } from '../utils/errors.dto';
import { AuthBodyDto, TokenDto } from './auth.dto';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './local-auth.guard';
import { User } from './user.decorator';
@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  @ApiOperation({
    summary: 'Login request, you will obtain a Bearer token',
  })
  @ApiBody({
    type: AuthBodyDto,
  })
  @ApiResponse({
    status: 201,
    type: TokenDto,
    description: 'Login success, the body contains the new Bearer token',
  })
  @ApiUnauthorizedResponse({
    status: 401,
    type: UnauthorizedDto,
    description: 'Invalid crendentials',
  })
  async login(@User() user: AuthBodyDto): Promise<TokenDto> {
    return this.authService.login(user);
  }
}
