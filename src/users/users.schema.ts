import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { Settings, SettingsDocument } from './settings/settings.schema';

export type UserDocument = User & Document;

@Schema({
  toObject: {
    transform: (doc, ret) => {
      delete ret.password;
    },
  },
  toJSON: {
    transform: (doc, ret) => {
      delete ret.password;
    },
  },
})
export class User {
  @Prop({
    required: true,
    unique: true,
    minlength: 3,
    maxlength: 32,
  })
  username: string;

  @Prop({
    required: true,
    minlength: 8,
    maxlength: 256,
  })
  password: string;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: Settings.name,
    required: true,
  })
  settings: SettingsDocument;
}

export const UserSchema = SchemaFactory.createForClass(User);
