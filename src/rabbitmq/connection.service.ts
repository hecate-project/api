import { Inject } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import * as amqplib from 'amqplib';
import * as Bluebird from 'bluebird';
import amqpConfig from '../config/amqp.config';

export const RABBIT_CONNECTION = 'RABBIT_CONNECTION';

export class RabbitMQConnectionService {
  private connectionPromise: Bluebird<void>;
  private connection: amqplib.Connection = null;

  constructor(
    @Inject(amqpConfig.KEY)
    private amqp: ConfigType<typeof amqpConfig>,
  ) {
    this.connectionPromise = amqplib.connect(this.amqp.amqpUrl).then((conn) => {
      this.connection = conn;
    });
  }

  async provide(): Promise<amqplib.Connection> {
    if (!this.connection) {
      await this.connectionPromise;
    }
    return this.connection;
  }
}
