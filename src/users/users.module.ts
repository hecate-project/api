import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SettingsService } from './settings/settings.service';
import { SettingsController } from './settings/settings.controller';
import { Settings, SettingsSchema } from './settings/settings.schema';
import { User, UserSchema } from './users.schema';
import { UsersService } from './users.service';
import { Exchanges, ExchangesSchema } from './settings/exchanges.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: User.name,
        schema: UserSchema,
      },
      {
        name: Settings.name,
        schema: SettingsSchema,
      },
      {
        name: Exchanges.name,
        schema: ExchangesSchema,
      },
    ]),
  ],
  providers: [UsersService, SettingsService],
  controllers: [SettingsController],
  exports: [UsersService, SettingsService],
})
export class UsersModule {}
