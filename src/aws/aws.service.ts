import { Inject, Injectable } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { S3 } from 'aws-sdk';
import awsConfig from '../config/aws.config';
import * as UUID from 'uuid';

export interface FileUploader {
  url: string;
  key: string;
}

@Injectable()
export class AwsService {
  private s3: S3;

  constructor(
    @Inject(awsConfig.KEY)
    private aws: ConfigType<typeof awsConfig>,
  ) {
    this.s3 = new S3({
      endpoint: this.aws.endpoint,
      credentials: {
        accessKeyId: this.aws.accessKeyId,
        secretAccessKey: this.aws.secretAccessKey,
      },
      signatureVersion: 'v4',
      region: this.aws.region,
    });
  }

  async getUploadUrl(folder: string, filename: string): Promise<FileUploader> {
    const key = `${folder}/${UUID.v4()}/${filename}`;
    const url = await this.s3.getSignedUrlPromise('putObject', {
      Bucket: this.aws.bucket,
      Key: key,
    });
    return {
      key,
      url,
    };
  }
}
