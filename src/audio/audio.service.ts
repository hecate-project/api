import { Injectable } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { AwsService } from '../aws/aws.service';

export class AudioUploaderDto {
  @ApiProperty({
    description: 'Url to upload your wav file, Only work with HTTP PUT request',
    example:
      'https://tek-hecate.s3.eu-west-3.amazonaws.com/audio/10c2877f-1d8c-4795-a0cc-6845bc071825/input.wav?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA2ZLa275LDSY5DFPV%2F20210930%2Feu-west-3%2Fs3%2Faws4_request&X-Amz-Date=20210930T0910339&X-Amz-Expires=90&X-Amz-Signature=1c7335ee914c0ba9863fdebf48ddb93aca48857f7a78c3deb169c148f4682476&X-Amz-SignedHeaders=host',
  })
  @IsString()
  readonly url: string;

  @ApiProperty({
    description: 'The S3 file location, provide her in another api request',
    example: 'audio/10c2877f-1d8x-4795-a0cc-6845bc0w1825/input.wav',
  })
  @IsString()
  readonly key: string;
}

@Injectable()
export class AudioService {
  constructor(private awsService: AwsService) {}

  async requestUploadUrl(): Promise<AudioUploaderDto> {
    return this.awsService.getUploadUrl('audio', 'input.wav');
  }
}
