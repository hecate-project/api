import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from './users.schema';
import * as bcrypt from 'bcrypt';
import { SettingsService } from './settings/settings.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    @Inject(forwardRef(() => SettingsService))
    private settingsService: SettingsService,
  ) {}

  async create(username: string, password: string): Promise<User> {
    // Check if username isn't already owned
    const usersFound = await this.userModel
      .find({
        username,
      })
      .exec();
    if (usersFound.length > 0) {
      throw new Error('Username already taken');
    }

    const saltRounds = 10;
    const passwordHashed = bcrypt.hashSync(password, saltRounds);

    const settings = await this.settingsService.create();
    return this.userModel.create({
      username,
      password: passwordHashed,
      settings,
    });
  }

  get model(): Model<UserDocument> {
    return this.userModel;
  }

  findByUsername(username: string): Promise<User | null> {
    return this.userModel
      .findOne({
        username,
      })
      .exec();
  }
}
