import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsString, Length } from 'class-validator';

export class SettingsDto {
  @IsString()
  @Length(1, 64)
  @ApiProperty({
    description: 'The angelie service rabbitMQ exchange',
    example: 'key-angelie/prod',
  })
  readonly 'angelie-exchange': string;

  @IsString()
  @Length(1, 64)
  @ApiProperty({
    description: 'The alce service rabbitMQ exchange',
    example: 'key-alce/prod',
  })
  readonly 'alce-exchange': string;

  @IsString()
  @Length(1, 64)
  @ApiProperty({
    description: 'The eole service rabbitMQ exchange',
    example: 'key-eole/prod',
  })
  readonly 'eole-exchange': string;
}

export class ExchangesAvailables {
  @ApiProperty({
    isArray: true,
    description: 'All angelie service exchanges availables',
    example: ['key-angelie/prod', 'key-angelie/gabouchet'],
  })
  readonly angelie: string[];

  @ApiProperty({
    isArray: true,
    description: 'All alce service exchanges availables',
    example: ['key-alce/prod', 'key-alce/gabouchet'],
  })
  readonly alce: string[];

  @ApiProperty({
    isArray: true,
    description: 'All eole service exchanges availables',
    example: ['key-eole/prod', 'key-eole/gabouchet'],
  })
  readonly eole: string[];
}
