import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ExchangesDocument = Exchanges & Document;

@Schema()
export class Exchanges {
  @Prop({
    required: true,
    default: ['key-angelie/prod'],
  })
  'angelie': string[];

  @Prop({
    required: true,
    default: ['key-alce/prod'],
  })
  'alce': string[];

  @Prop({
    required: true,
    default: ['key-eole/prod'],
  })
  'eole': string[];
}

export const ExchangesSchema = SchemaFactory.createForClass(Exchanges);
