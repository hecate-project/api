import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: true,
  });

  // OpenAPI
  const config = new DocumentBuilder()
    .setTitle('Hécate')
    .setDescription('OpenAPI document for Hécate API')
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('openapi', app, document);

  // For class-transormer
  app.useGlobalPipes(new ValidationPipe());

  await app.listen(3000);
}
bootstrap();
