import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type SettingsDocument = Settings & Document;

@Schema()
export class Settings {
  @Prop({
    required: true,
    default: 'key-angelie/prod',
  })
  'angelie-exchange': string;

  @Prop({
    required: true,
    default: 'key-alce/prod',
  })
  'alce-exchange': string;

  @Prop({
    required: true,
    default: 'key-eole/prod',
  })
  'eole-exchange': string;
}

export const SettingsSchema = SchemaFactory.createForClass(Settings);
