import { Module } from '@nestjs/common';
import { RabbitMQModule } from '../rabbitmq/rabbitmq.module';
import { AlceService } from './alce.service';
import { AngelieService } from './angelie.service';
import { EoleService } from './eole.service';

@Module({
  imports: [RabbitMQModule],
  providers: [AngelieService, AlceService, EoleService],
  exports: [AngelieService, AlceService, EoleService],
})
export class ServicesModule {}
