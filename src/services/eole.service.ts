import { Injectable } from '@nestjs/common';
import { ResponseEngineService } from '../rabbitmq/responses.service';
import { RabbitMQService } from '../rabbitmq/rabbitmq.service';
import { AbstractService } from './abstract.service';

export interface EoleRequest {
  readonly from: string;
  readonly to: string;
}

export interface EoleResponse {
  // TODO DEFINE
  readonly points: string;
}

@Injectable()
export class EoleService extends AbstractService<EoleRequest, EoleResponse> {
  constructor(
    rabbitMQService: RabbitMQService,
    responsesEngine: ResponseEngineService,
  ) {
    super(rabbitMQService, responsesEngine, 'queue-eole-api');
  }
}
