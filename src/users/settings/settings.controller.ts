import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { UserPayloadDto } from '../../auth/auth.dto';
import { User } from '../../auth/user.decorator';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { ExchangesAvailables, SettingsDto } from './settings.dto';
import { SettingsService } from './settings.service';
import { UnauthorizedDto } from '../../utils/errors.dto';

@Controller('user/settings')
@ApiTags('Settings')
@ApiBearerAuth()
export class SettingsController {
  constructor(private settingsService: SettingsService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  @ApiOperation({
    summary: 'Get the user settings',
  })
  @ApiResponse({
    status: 200,
    type: SettingsDto,
    description: 'The user settings',
  })
  @ApiUnauthorizedResponse({
    status: 401,
    type: UnauthorizedDto,
    description: 'User not authenticated',
  })
  async getMySettings(@User() user: UserPayloadDto): Promise<SettingsDto> {
    return this.settingsService.getByUser(user.id);
  }

  @UseGuards(JwtAuthGuard)
  @Get('exchanges')
  @ApiOperation({
    summary: 'Get all services exchanges availables',
  })
  @ApiResponse({
    status: 200,
    type: ExchangesAvailables,
    description: 'Exchanges available sorted by services',
  })
  @ApiUnauthorizedResponse({
    status: 401,
    type: UnauthorizedDto,
    description: 'User not authenticated',
  })
  async getAvailableExchanges(): Promise<ExchangesAvailables> {
    const exchanges = (
      await this.settingsService.getAvailablesExchanges()
    ).toObject({ versionKey: false });
    delete exchanges._id;
    return exchanges;
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  @ApiOperation({
    summary: 'Update the user settings',
  })
  @ApiUnauthorizedResponse({
    status: 401,
    type: UnauthorizedDto,
    description: 'User not authenticated',
  })
  @ApiResponse({
    status: 201,
    description: 'User settings updated',
  })
  @ApiBody({
    type: SettingsDto,
  })
  async updateMySettings(
    @User() user: UserPayloadDto,
    @Body() settings: SettingsDto,
  ): Promise<void> {
    return await this.settingsService.updateByUser(user.id, settings);
  }
}
