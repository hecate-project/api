import { Injectable } from '@nestjs/common';
import { ResponseStatus } from '../rabbitmq/responses.service';
import {
  AlceRequest,
  AlceResponse,
  AlceService,
} from '../services/alce.service';
import {
  EoleRequest,
  EoleResponse,
  EoleService,
} from '../services/eole.service';
import { UserPayloadDto } from '../auth/auth.dto';
import {
  AngelieRequest,
  AngelieResponse,
  AngelieService,
} from '../services/angelie.service';
import { SettingsDto } from '../users/settings/settings.dto';
import { handleError, ResponseDto } from './dto/response.dto';
import { Response } from '../rabbitmq/responses.service';
import { RequestDto } from './dto/request.dto';

export enum RequestType {
  audio = 'audio',
  text = 'text',
}

export enum LanguageSupported {
  fr = 'fr',
}
@Injectable()
export class RequestService {
  constructor(
    private angelieService: AngelieService,
    private alceService: AlceService,
    private eoleService: EoleService,
  ) {}

  async newRequest(
    user: UserPayloadDto,
    request: RequestDto,
    settings: SettingsDto,
  ): Promise<ResponseDto> {
    let angelieResponse: Response<AngelieRequest, AngelieResponse> | null =
      null;
    let alceResponse: Response<AlceRequest, AlceResponse> | null = null;
    let eoleResponse: Response<EoleRequest, EoleResponse> | null = null;
    let textContent = request.content;

    // If audio, use angelie to decode it
    if (request.type === RequestType.audio) {
      const angelieRequest = this.angelieService.request(
        settings['angelie-exchange'],
        {
          language: request.language,
          s3Key: request.content,
        },
      );
      angelieResponse = await this.angelieService.waitFor(angelieRequest);
      if (angelieResponse.status === ResponseStatus.Error)
        return handleError(angelieResponse, alceResponse, eoleResponse);
      textContent = angelieResponse.body.text;
    }

    // Process alce to find direction
    const alceRequest = this.alceService.request(settings['alce-exchange'], {
      language: request.language,
      text: textContent,
    });
    alceResponse = await this.alceService.waitFor(alceRequest);
    if (alceResponse.status === ResponseStatus.Error)
      return handleError(angelieResponse, alceResponse, eoleResponse);

    // Call eole to resolve map itinary
    const eoleRequest = await this.eoleService.request(
      settings['eole-exchange'],
      {
        ...alceResponse.body,
      },
    );
    eoleResponse = await this.eoleService.waitFor(eoleRequest);
    return handleError(angelieResponse, alceResponse, eoleResponse);
  }
}
