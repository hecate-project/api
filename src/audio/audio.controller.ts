import { Controller, Get, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { UnauthorizedDto } from '../utils/errors.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { AudioService, AudioUploaderDto } from './audio.service';

@Controller('audio')
@ApiTags('Audio')
@ApiBearerAuth()
export class AudioController {
  constructor(private audioService: AudioService) {}

  @UseGuards(JwtAuthGuard)
  @Get('upload')
  @ApiOperation({
    summary: 'Request a S3 upload url, to upload your wav file',
  })
  @ApiUnauthorizedResponse({
    status: 401,
    type: UnauthorizedDto,
    description: 'User not authenticated',
  })
  @ApiResponse({
    status: 200,
    type: AudioUploaderDto,
    description: 'The AWS S3 object for upload purpose',
  })
  requestUpload(): Promise<AudioUploaderDto> {
    return this.audioService.requestUploadUrl();
  }
}
