import { Inject, Injectable } from '@nestjs/common';
import * as amqplib from 'amqplib';
import { RABBIT_CONNECTION } from './connection.service';
import { Request } from '../services/abstract.service';
import { ResponseEngineService } from './responses.service';

@Injectable()
export class RabbitMQService {
  private channelPromise: Promise<void> | null;
  private channel: amqplib.Channel;

  constructor(
    @Inject(RABBIT_CONNECTION) private conn: amqplib.Connection,
    private responsesEngine: ResponseEngineService,
  ) {
    this.channelPromise = new Promise((resolve, reject) => {
      this.conn
        .createChannel()
        .then((ch) => {
          this.channel = ch;
          resolve();
        })
        .catch(reject);
    });
  }

  private async verifChannel() {
    if (this.channelPromise) {
      await this.channelPromise;
    }
  }

  async publish<Req>(
    exchange: string,
    routingKey: string,
    request: Request<Req>,
  ) {
    await this.verifChannel();
    const result = this.channel.publish(
      exchange,
      routingKey,
      Buffer.from(
        JSON.stringify({
          ...request.body,
          requestId: request.id,
        }),
      ),
    );
    if (result === false) throw new Error(`Failed to publish to '${exchange}'`);
  }

  async newConsumer(responseQueue: string) {
    await this.verifChannel();
    this.channel.consume(responseQueue, (msg) => {
      if (msg === null) return;
      try {
        const response = JSON.parse(msg.content.toString()) as any;
        this.channel.ack(msg);
        this.responsesEngine.resolve(response);
      } catch (e) {
        console.error('Failed to parse response');
      }
    });
  }
}
