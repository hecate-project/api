import { ApiProperty } from '@nestjs/swagger';
import { AlceRequest, AlceResponse } from '../../services/alce.service';
import {
  AngelieRequest,
  AngelieResponse,
} from '../../services/angelie.service';
import { EoleRequest, EoleResponse } from '../../services/eole.service';
import { ResponseStatus, Response } from '../../rabbitmq/responses.service';

export class ResponseDto {
  @ApiProperty({
    description: 'The global state of the whole request',
    example: ResponseStatus.Done,
    enum: ResponseStatus,
  })
  status: ResponseStatus;

  @ApiProperty({
    description: 'Global status message of the whole request',
    example: 'Unable to decode your audio file',
  })
  message: string;

  @ApiProperty({
    description: 'Angelie service response',
  })
  angelie: Response<AngelieRequest, AngelieResponse> | null;

  @ApiProperty({
    description: 'Alce service response',
  })
  alce: Response<AlceRequest, AlceResponse> | null;

  @ApiProperty({
    description: 'Eole service response',
  })
  eole: Response<EoleRequest, EoleResponse> | null;
}

export function handleError(
  angelie: Response<AngelieRequest, AngelieResponse> | null,
  alce: Response<AlceRequest, AlceResponse> | null,
  eole: Response<EoleRequest, EoleResponse> | null,
): ResponseDto {
  const result: ResponseDto = {
    status: ResponseStatus.Done,
    message: 'OK',
    angelie: angelie,
    alce: alce,
    eole: eole,
  };

  if (angelie && angelie.status === ResponseStatus.Error) {
    (result.status = ResponseStatus.Error),
      (result.message = 'Unable to decode your audio file');
    return result;
  }

  if (alce && alce.status === ResponseStatus.Error) {
    (result.status = ResponseStatus.Error),
      (result.message = 'Unable to extract direction from your text');
    return result;
  }

  if (eole && eole.status === ResponseStatus.Error) {
    (result.status = ResponseStatus.Error),
      (result.message = 'Unable to determines itinary');
    return result;
  }

  return result;
}
