FROM node:19.2.0 as builder

# Update npm version
RUN npm install -g npm

WORKDIR /build

COPY src src
COPY ["package.json", "package-lock.json", "tsconfig.json", "tsconfig.build.json", "./"]

# Install dependencies and build the app
RUN npm i
RUN npm run build

# Clean useless dependencies
RUN npm prune --production

FROM node:19.2.0-alpine3.15

# Update npm version
RUN npm install -g npm

WORKDIR /app

COPY --from=builder /build/dist dist
COPY --from=builder /build/node_modules node_modules

EXPOSE 3000

ENTRYPOINT ["node", "dist/main.js"]
