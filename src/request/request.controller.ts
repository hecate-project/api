import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { UnauthorizedDto } from '../utils/errors.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { RequestService } from './request.service';
import { User } from '../auth/user.decorator';
import { UserPayloadDto } from '../auth/auth.dto';
import { SettingsService } from '../users/settings/settings.service';
import { RequestDto } from './dto/request.dto';
import { ResponseDto } from './dto/response.dto';

@Controller('request')
@ApiTags('Request')
@ApiBearerAuth()
export class RequestController {
  constructor(
    private requestService: RequestService,
    private settingsService: SettingsService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Post('')
  @ApiOperation({
    summary: 'Request the travel decoder',
  })
  @ApiUnauthorizedResponse({
    status: 401,
    type: UnauthorizedDto,
    description: 'User not authenticated',
  })
  @ApiResponse({
    status: 201,
    type: ResponseDto,
    description: 'Request accepted',
  })
  async requestSpeechToDirection(
    @User() user: UserPayloadDto,
    @Body() request: RequestDto,
  ): Promise<ResponseDto> {
    const settings = await this.settingsService.getByUser(user.id);
    return this.requestService.newRequest(user, request, settings);
  }
}
