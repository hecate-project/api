import { Injectable } from '@nestjs/common';
import { User } from '../users/users.schema';
import { UsersService } from '../users/users.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { TokenDto } from './auth.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(username: string, password: string): Promise<User | null> {
    const user = await this.usersService.findByUsername(username);
    if (user && bcrypt.compareSync(password, user.password)) {
      return user;
    }
    return null;
  }

  async login(user: any): Promise<TokenDto> {
    const { _id, username } = user.toObject();
    return {
      access_token: this.jwtService.sign({
        userId: _id,
        username,
      }),
    };
  }
}
