import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import amqpConfig from '../config/amqp.config';
import {
  RabbitMQConnectionService,
  RABBIT_CONNECTION,
} from './connection.service';
import { RabbitMQService } from './rabbitmq.service';
import { ResponseEngineService } from './responses.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [amqpConfig],
    }),
  ],
  providers: [
    RabbitMQConnectionService,
    {
      provide: RABBIT_CONNECTION,
      inject: [RabbitMQConnectionService],
      useFactory: async (connService: RabbitMQConnectionService) =>
        connService.provide(),
    },
    RabbitMQService,
    ResponseEngineService,
  ],
  exports: [RabbitMQService, ResponseEngineService],
})
export class RabbitMQModule {}
