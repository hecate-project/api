import { Module } from '@nestjs/common';
import { AwsModule } from '../aws/aws.module';
import { AudioController } from './audio.controller';
import { AudioService } from './audio.service';

@Module({
  imports: [AwsModule],
  controllers: [AudioController],
  providers: [AudioService],
})
export class AudioModule {}
