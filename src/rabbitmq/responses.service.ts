import { Injectable } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { Request } from '../services/abstract.service';

export enum ResponseStatus {
  Done = 'done',
  Error = 'error',
}

interface ResponsePayload {
  requestId: string;
  status: ResponseStatus;
  message: string;
}

export class Response<Req, Res> {
  public request: Request<Req> | null = null;

  @ApiProperty({
    description: 'The service response status',
    example: ResponseStatus.Done,
    enum: ResponseStatus,
  })
  public readonly status: ResponseStatus;

  @ApiProperty({
    description: 'The service response message',
    example: 'OK',
  })
  public readonly message: string;

  @ApiProperty({
    description: 'The request/response id (will be the same)',
    example: '10c2877f-1d8x-4795-a0cc-6845bc0w1825',
  })
  public readonly id: string;

  @ApiProperty({
    description: 'The service response body',
  })
  public readonly body: Res;

  constructor(readonly payload: ResponsePayload) {
    this.status = payload.status;
    this.id = payload.requestId;
    this.message = payload.message;
    delete payload.status;
    delete payload.requestId;
    delete payload.message;
    this.body = payload as any;
  }
}

@Injectable()
export class ResponseEngineService {
  private waitingRequests: Map<string, Request<any>> = new Map<
    string,
    Request<any>
  >();

  private resolvedRequests: Map<string, Response<any, any>> = new Map<
    string,
    Response<any, any>
  >();

  push(request: Request<any>) {
    this.waitingRequests.set(request.id, request);
  }

  resolve(body: any) {
    const response = new Response<any, any>(body);
    if (!this.waitingRequests.has(response.id)) {
      console.error('Unable to find request associated');
      return;
    }
    response.request = this.waitingRequests.get(response.id);
    this.waitingRequests.delete(response.id);
    this.resolvedRequests.set(response.id, response);
  }

  waitFor<Req, Res>(request: Request<Req>): Promise<Response<Req, Res>> {
    return new Promise((resolve) => {
      setInterval(() => {
        if (this.resolvedRequests.has(request.id)) {
          const resolved = this.resolvedRequests.get(request.id);
          this.resolvedRequests.delete(request.id);
          resolve(resolved);
        }
      }, 500);
    });
  }
}
