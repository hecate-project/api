import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsString, Length } from 'class-validator';
import { LanguageSupported, RequestType } from '../request.service';

export class RequestDto {
  @ApiProperty({
    description:
      'The decode mode, choose if the api need to decode audio or text',
    examples: [RequestType.audio, RequestType.text],
    enum: RequestType,
  })
  @IsEnum(RequestType)
  type: RequestType;

  @ApiProperty({
    description: 'The input language',
    example: 'fr',
    enum: LanguageSupported,
  })
  @IsEnum(LanguageSupported)
  language: LanguageSupported;

  @ApiProperty({
    description:
      'Can be the S3 file key or the text to be decoded. Depend on the request type',
    example: 'audio/10c2877f-1d8x-4795-a0cc-6845bc0w1825/input.wav',
    examples: [
      'audio/10c2877f-1d8x-4795-a0cc-6845bc0w1825/input.wav',
      'Je souhaite aller à la Gare Montparnasse',
    ],
  })
  @IsString()
  @Length(1, 4096)
  content: string;
}
