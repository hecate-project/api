import { ApiProperty } from '@nestjs/swagger';
import { IsString, Length } from 'class-validator';

export class UserPayloadDto {
  @ApiProperty({
    example: '507f191e810c19729de860ea',
  })
  @IsString()
  @Length(1, 256)
  readonly id: string;

  @ApiProperty({
    example: 'gabouchet',
  })
  @IsString()
  @Length(3, 32)
  readonly username: string;
}
export class AuthBodyDto {
  @ApiProperty({
    example: 'gabouchet',
    description: 'The username of the user',
  })
  @IsString()
  @Length(3, 32)
  readonly username: string;

  @ApiProperty({
    example: 'strongpassword',
    description: 'The password of the user',
  })
  @IsString()
  @Length(8, 256)
  readonly password: string;
}

export class TokenDto {
  @ApiProperty({
    example:
      'eyJhbGciOiJIxzI1NiInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MTU0YzI4MDViNTJkhmYzM3ODRvNTIiLCJ1c2VybmFtZSI6ImdhYnJpZWwiLCJpYXQiOjE2MzI5OTE1MjksImV4cCI6MbYzODE3NTUyOX0.GRr2rD3_dB625Cn4g2bqqb9HsMFbq3zgkmoQkyURB1w',
    description: 'JWT token, as Bearer format',
  })
  @IsString()
  readonly access_token: string;
}
