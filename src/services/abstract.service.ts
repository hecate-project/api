import { RabbitMQService } from '../rabbitmq/rabbitmq.service';
import * as UUID from 'uuid';
import * as moment from 'moment';
import { ResponseEngineService } from '../rabbitmq/responses.service';

export class Request<Body> {
  public readonly id = UUID.v4();
  public readonly date = moment();

  constructor(public readonly body: Body) {}
}

export abstract class AbstractService<Req, Res> {
  constructor(
    private rabbitMQService: RabbitMQService,
    private responsesEngine: ResponseEngineService,
    responseQueue: string,
  ) {
    this.rabbitMQService.newConsumer(responseQueue);
  }

  request(exchange: string, body: Req): Request<Req> {
    const request = new Request<Req>(body);
    this.responsesEngine.push(request);
    this.rabbitMQService.publish<Req>('amq.direct', exchange, request);
    return request;
  }

  waitFor(request: Request<Req>) {
    return this.responsesEngine.waitFor<Req, Res>(request);
  }
}
