import { Injectable } from '@nestjs/common';
import { ResponseEngineService } from '../rabbitmq/responses.service';
import { RabbitMQService } from '../rabbitmq/rabbitmq.service';
import { LanguageSupported } from '../request/request.service';
import { AbstractService } from './abstract.service';

export interface AlceRequest {
  readonly language: LanguageSupported;
  readonly text: string;
}

export interface AlceResponse {
  readonly from: string;
  readonly to: string;
}

@Injectable()
export class AlceService extends AbstractService<AlceRequest, AlceResponse> {
  constructor(
    rabbitMQService: RabbitMQService,
    responsesEngine: ResponseEngineService,
  ) {
    super(rabbitMQService, responsesEngine, 'queue-alce-api');
  }
}
