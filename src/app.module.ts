import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService, ConfigType } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AudioModule } from './audio/audio.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import dbConfig from './config/db.config';
import { RabbitMQModule } from './rabbitmq/rabbitmq.module';
import { RequestModule } from './request/request.module';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [
        ConfigModule.forRoot({
          load: [dbConfig],
        }),
      ],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<ConfigType<typeof dbConfig>>('db').mongoUrl,
        useCreateIndex: true,
        useFindAndModify: false,
      }),
    }),
    AudioModule,
    AuthModule,
    UsersModule,
    RabbitMQModule,
    RequestModule,
  ],
})
export class AppModule {}
