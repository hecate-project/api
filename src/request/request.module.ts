import { Module } from '@nestjs/common';
import { ServicesModule } from '../services/services.module';
import { UsersModule } from '../users/users.module';
import { RequestController } from './request.controller';
import { RequestService } from './request.service';

@Module({
  imports: [UsersModule, ServicesModule],
  controllers: [RequestController],
  providers: [RequestService],
})
export class RequestModule {}
