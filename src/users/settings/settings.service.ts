import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Settings, SettingsDocument } from './settings.schema';
import { UsersService } from '../users.service';
import { SettingsDto } from './settings.dto';
import { Exchanges, ExchangesDocument } from './exchanges.schema';

@Injectable()
export class SettingsService {
  constructor(
    @InjectModel(Settings.name) private settingsModel: Model<SettingsDocument>,
    @InjectModel(Exchanges.name)
    private exchangesModel: Model<ExchangesDocument>,
    private userService: UsersService,
  ) {
    // Create default exchanges settings if they not exists
    this.getAvailablesExchanges().then((exchanges) => {
      if (exchanges === null) {
        this.exchangesModel.create({});
      }
    });
  }

  create(): Promise<Settings> {
    return this.settingsModel.create({});
  }

  async getByUser(userId: string): Promise<SettingsDto> {
    const user = await this.userService.model
      .findById(userId)
      .populate('settings')
      .exec();
    const settings = user.settings.toObject({
      versionKey: false,
    });
    delete settings._id;
    return settings;
  }

  async updateByUser(userId: string, settings: SettingsDto): Promise<void> {
    // Get the settingsId in user
    const user = await this.userService.model.findById(userId).exec();
    await this.settingsModel.findByIdAndUpdate(user.settings, settings).exec();
  }

  getAvailablesExchanges(): Promise<ExchangesDocument | null> {
    return this.exchangesModel.findOne().exec();
  }
}
