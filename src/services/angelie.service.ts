import { Injectable } from '@nestjs/common';
import { ResponseEngineService } from '../rabbitmq/responses.service';
import { RabbitMQService } from '../rabbitmq/rabbitmq.service';
import { LanguageSupported } from '../request/request.service';
import { AbstractService } from './abstract.service';
export interface AngelieRequest {
  readonly language: LanguageSupported;
  readonly s3Key: string;
}

export class AngelieResponse {
  readonly text: string;
}

@Injectable()
export class AngelieService extends AbstractService<
  AngelieRequest,
  AngelieResponse
> {
  constructor(
    rabbitMQService: RabbitMQService,
    responsesEngine: ResponseEngineService,
  ) {
    super(rabbitMQService, responsesEngine, 'queue-angelie-api');
  }
}
