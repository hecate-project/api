import { registerAs } from '@nestjs/config';

export default registerAs('amqp', () => ({
  amqpUrl: process.env.AMQP_URL,
}));
