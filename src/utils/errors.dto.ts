import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';

export class UnauthorizedDto {
  @IsNumber()
  @ApiProperty({
    example: 401,
    description: 'The http status code',
  })
  readonly statusCode: number;

  @IsString()
  @ApiProperty({
    example: 'Unauthorized',
    description: 'The error message',
  })
  readonly message: string;
}
